package com.cisco.shopping.app.adapter;

import android.annotation.SuppressLint;
import android.content.ClipData;
import android.content.ClipDescription;
import android.content.Context;
import android.graphics.Color;
import android.os.Build;
import android.util.Log;
import android.view.DragEvent;
import android.view.LayoutInflater;
import android.view.View.OnTouchListener;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.cisco.shopping.app.R;
import com.cisco.shopping.app.helper.DB_Handler;
import com.cisco.shopping.app.helper.MyDragShadowBuilder;

import java.util.List;

import androidx.recyclerview.widget.RecyclerView;

public class ItemsAdapter extends RecyclerView.Adapter<ItemsViewHolder> {
    private Context context;
    private LayoutInflater inflater;
    private List<String> itemList;
    private List<String> itemColour;
    private ImageView imgCart;
    private TextView tvCount;
    private String sItemName="";
    private int position;
    private float width = 0;
    private float height = 0;
    String msg = "##DragLn";
    private static final String IMAGEVIEW_TAG = "DRAGGABLE_TEXT";
    private static final String LINEAR_TAG = "DRAGGABLE_LINEAR";
    private DB_Handler db_handler;
    private int cartCount = 0;

    public ItemsAdapter(Context context, int position, List<String> itemList, List<String> itemColour, float width, float height, ImageView imgCart, TextView tvCount) {
        this.context = context;
        this.itemList = itemList;
        this.itemColour = itemColour;
        this.imgCart = imgCart;
        this.tvCount = tvCount;
        this.width = width;
        this.height = height;
        this.position = position;

        db_handler = new DB_Handler(context);

        //inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }


    @Override
    public ItemsViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item, parent, false);

        ItemsViewHolder viewHolder = new ItemsViewHolder(view);
        return viewHolder;
    }

    @SuppressLint("RecyclerView")
    @Override
    public void onBindViewHolder(ItemsViewHolder holder, int position) {

        holder.tvItemName.setText(itemList.get(position));
        holder.tvItemName.setBackgroundColor(Color.parseColor(itemColour.get(position)));

        cartCount = db_handler.getCartItemCount();

        if (cartCount > 0) {
            tvCount.setText(String.valueOf(cartCount));
            Log.e("###count", String.valueOf(cartCount));
        } else {
            tvCount.setText("0");
        }

        //holder.tvItemName.getLayoutParams().width = (int) width;
        holder.tvItemName.getLayoutParams().height = (int) height;

        // Set the tag
        holder.tvItemName.setTag(IMAGEVIEW_TAG);
        holder.lnItem.setTag(LINEAR_TAG);

        holder.lnItem.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                // Create a new ClipData.
                ClipData.Item item = new ClipData.Item((CharSequence)v.getTag());

                // Create a new ClipData using the tag as a label, the plain text MIME type, and
                // the already-created item. This creates a new ClipDescription object within the
                // ClipData and sets its MIME type to "text/plain".
                ClipData dragData = new ClipData(
                        (CharSequence) v.getTag(),
                        new String[] { ClipDescription.MIMETYPE_TEXT_PLAIN },
                        item);

                // Instantiate the drag shadow builder.
                View.DragShadowBuilder myShadow = new MyDragShadowBuilder(holder.lnItem);

                // Start the drag.
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                    v.startDragAndDrop(dragData,  // The data to be dragged
                            myShadow,  // The drag shadow builder
                            null,      // No need to use local data
                            0          // Flags (not currently used, set to 0)
                    );
                }
                // Indicate that the long-click was handled.
                return true;
            }
        });

        holder.lnItem.setOnDragListener(new View.OnDragListener() {
            @Override
            public boolean onDrag(View v, DragEvent event) {
                switch(event.getAction()) {
                    case DragEvent.ACTION_DRAG_STARTED:

                        String clipData = event.getClipDescription().getLabel().toString();

                        Log.d(msg+"START", clipData);

                        // Determines if this View can accept the dragged data.
                        if (event.getClipDescription().hasMimeType(ClipDescription.MIMETYPE_TEXT_PLAIN)) {

                            // Invalidate the view to force a redraw in the new tint.
                            v.invalidate();

                            // Returns true to indicate that the View can accept the dragged data.
                            return true;
                        }

                        // Returns false to indicate that, during the current drag and drop operation,
                        // this View will not receive events again until ACTION_DRAG_ENDED is sent.
                        return false;

                    case DragEvent.ACTION_DRAG_ENTERED:

                        clipData = event.getClipDescription().getLabel().toString();
                        Log.d(msg+"ENTERED", clipData);


                        // Invalidates the view to force a redraw in the new tint.
                        v.invalidate();

                        // Returns true; the value is ignored.
                        return true;

                    case DragEvent.ACTION_DRAG_LOCATION:

                        // Ignore the event.
                        return true;

                    case DragEvent.ACTION_DRAG_EXITED:

                        // Invalidates the view to force a redraw in the new tint.
                        v.invalidate();

                        // Returns true; the value is ignored.
                        return true;

                    case DragEvent.ACTION_DROP:

                        clipData = event.getClipDescription().getLabel().toString();
                        Log.d(msg+"DROP", clipData);

                        // Gets the item containing the dragged data.
                        ClipData.Item item = event.getClipData().getItemAt(0);

                        // Does a getResult(), and displays what happened.
                        if (event.getResult()) {
                            Log.d(msg+"DROP", "The drop was handled");
                        } else {
                            Log.d(msg+"DROP", "The drop didn't work");
                        }

                        // Invalidates the view to force a redraw.
                        v.invalidate();

                        // Returns true. DragEvent.getResult() will return true.
                        return true;

                    case DragEvent.ACTION_DRAG_ENDED:

                        // Invalidates the view to force a redraw.
                        v.invalidate();

                        // Does a getResult(), and displays what happened.
                        if (event.getResult()) {
                            Log.d(msg+"END", "The drag_end was handled");
                        } else {
                            Log.d(msg+"END", "The drag_end didn't work");
                        }

                        if(imgCart.isInTouchMode()){
                            sItemName = itemList.get(position);

                            Log.e("##@@Item", sItemName);

                            if (isSuccessAddingToCart(sItemName)) {
                                Toast.makeText(context, "Item Added!", Toast.LENGTH_SHORT).show();
                            }

                            Log.d(msg+"END", "FOCUSED");
                        }
                        else {
                            Log.d(msg+"END", "NOT FOCUSED");
                        }

                        if (cartCount > 0) {
                            tvCount.setText(String.valueOf(cartCount));
                            Log.e("###count", String.valueOf(cartCount));
                        } else {
                            tvCount.setText("0");
                        }

                        // Returns true; the value is ignored.
                        return true;

                    // An unknown action type was received.
                    default:
                        Log.e("DragDrop Example","Unknown action type received by View.OnDragListener.");
                        break;
                }

                return false;
            }
        });

        // defines touch listener
        holder.lnItem.setOnTouchListener(new OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_DOWN) {
                    ClipData data = ClipData.newPlainText("" , "");
                    View.DragShadowBuilder shadowBuilder = new View.DragShadowBuilder(holder.lnItem);
                    v.startDrag(data , shadowBuilder , v , 0);
                    v.setVisibility(View.VISIBLE);
                    return true;
                } else {
                    return false;
                }
            }
        });

    }

    @Override
    public int getItemCount() {
        return itemList.size();
    }

    // Get Item Adding To Cart Status
    private boolean isSuccessAddingToCart(String sItemName) {
        try {
            long result = db_handler.insertShoppingCart(sItemName);

            if (result > 0) {
                return true;
            } else {
                Toast.makeText(context, "Item Already Added!", Toast.LENGTH_SHORT).show();
                return false;
            }
        } catch (NullPointerException e) {
            return false;
        }
    }
}