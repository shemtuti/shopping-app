package com.cisco.shopping.app.adapter;

import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.cisco.shopping.app.R;

import androidx.recyclerview.widget.RecyclerView;

public class ItemsViewHolder extends RecyclerView.ViewHolder {
    public TextView tvItemName;
    public LinearLayout lnItem;

    public ItemsViewHolder(View itemView) {
        super(itemView);
        tvItemName = (TextView) itemView.findViewById(R.id.tvItemName);
        lnItem = (LinearLayout) itemView.findViewById(R.id.lnItem);

    }
}
