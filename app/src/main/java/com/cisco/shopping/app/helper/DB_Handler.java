package com.cisco.shopping.app.helper;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;
import android.widget.Toast;

import com.cisco.shopping.app.adapter.ItemsInCart;

import java.util.ArrayList;
import java.util.List;


public class DB_Handler extends SQLiteOpenHelper {

    // Database Version
    private static final int DATABASE_VERSION = 1;

    // Database Name
    private static final String DATABASE_NAME = "SHOP_APP";

    // Column Names Static Variables
    private static final String ITEM_NAME = "item_name";

    // Table Name Static Variables
    private static final String ShoppingTable = "shopping_list";

    // Create Shopping Table
    private static final String CREATE_SHOPPING_TABLE = "CREATE TABLE " + ShoppingTable + "("
            + ITEM_NAME + " TEXT NOT NULL" + ")";

    public DB_Handler(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }


    // Creating tables
    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(CREATE_SHOPPING_TABLE);
    }

    // Upgrading database
    @Override
    public void onUpgrade(SQLiteDatabase db, int i, int i1) {
        // Drop older table if existed
        db.execSQL("DROP TABLE IF EXISTS " + ShoppingTable);
    }


    // Get Cart Item Count
    public int getCartItemCount() {
        // Select All Query
        String selectQuery = "SELECT * FROM " + ShoppingTable;

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        int count = cursor.getCount();
        cursor.close();
        db.close();

        return count;
    }

    // Clear Items in Cart
    public void deleteItems() {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(ShoppingTable, null, null);
    }

    // Insert Item Into Cart
    public long insertShoppingCart(String sItemName) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(ITEM_NAME, sItemName);

        Log.e("##@@InsertItemName", sItemName);

        // Check If Value Already Exists
        String selectQuery = "SELECT * FROM " + ShoppingTable + " WHERE " + ITEM_NAME + " =?";
        Cursor cursor = db.rawQuery(selectQuery, new String[]{sItemName});
        if (cursor.moveToFirst()) {
            cursor.close();
            return -1;
        }
        cursor.close();
        return db.insert(ShoppingTable, null, values);
    }


    // Get items in Cart
    public ArrayList<ItemsInCart> getAllData() {
        ArrayList<ItemsInCart> itemlist = new ArrayList<>();
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor res = db.rawQuery("select * from "+ShoppingTable,null);

        while(res.moveToNext()) {
            String name = res.getString(0);

            ItemsInCart item = new ItemsInCart(name);
            itemlist.add(item);
        }
        return itemlist;
    }
}


