package com.cisco.shopping.app.activity;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.ListView;

import com.cisco.shopping.app.R;
import com.cisco.shopping.app.adapter.CartAdapter;
import com.cisco.shopping.app.adapter.ItemsInCart;
import com.cisco.shopping.app.helper.DB_Handler;

import java.util.ArrayList;

public class CartItemsActivity extends AppCompatActivity {
    private DB_Handler db_handler;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cart_list);
        fillListview();

    }

    public void fillListview() {
        ListView myListview = findViewById(R.id.list_items);
        db_handler = new DB_Handler(this);

        ArrayList<ItemsInCart> cartList = db_handler.getAllData();

        CartAdapter myAdapter = new CartAdapter(cartList, this);
        myListview.setAdapter(myAdapter);
    }
}