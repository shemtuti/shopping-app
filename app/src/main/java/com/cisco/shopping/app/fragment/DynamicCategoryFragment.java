package com.cisco.shopping.app.fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.ImageView;
import android.widget.TextView;

import com.cisco.shopping.app.R;
import com.cisco.shopping.app.adapter.ItemsAdapter;

import java.util.List;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

public class DynamicCategoryFragment extends Fragment {
    View view;
    RecyclerView recyclerView;
    ItemsAdapter adapter;
    int position;
    static List<String> itemList;
    static List<String> itemColour;
    static ImageView imgCart;
    static TextView tvCount;
    private float width;
    private float height;

    public static DynamicCategoryFragment getInstance(int position, List<String> itemList, List<String> itemColour, ImageView imgCart, TextView tvCount) {
        DynamicCategoryFragment.itemList = itemList;
        DynamicCategoryFragment.itemColour = itemColour;
        DynamicCategoryFragment.imgCart = imgCart;
        DynamicCategoryFragment.tvCount = tvCount;
        DynamicCategoryFragment.itemColour = itemColour;
        DynamicCategoryFragment f = new DynamicCategoryFragment ();

        // Supply num input as an argument.
        Bundle args = new Bundle();
        args.putInt("position", position);
        f.setArguments(args);

        return f;
    }

    public DynamicCategoryFragment () {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        position = getArguments() != null ? getArguments().getInt("position") : 1;

    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_category, container, false);

        return view;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        recyclerView = view.findViewById(R.id.recyclerView);
        recyclerView.setHasFixedSize(true);

        // setting grid layout manager to implement grid view.
        // in this method '2' represents number of columns to be displayed in grid view.
        GridLayoutManager layoutManager = new GridLayoutManager(getActivity(),2);

        recyclerView.setLayoutManager(layoutManager);

        recyclerView.getViewTreeObserver().addOnPreDrawListener(new ViewTreeObserver.OnPreDrawListener() {
            @Override
            public boolean onPreDraw() {
                if (recyclerView.getViewTreeObserver().isAlive())
                    recyclerView.getViewTreeObserver().removeOnPreDrawListener(this);

                width = (float) recyclerView.getWidth() / 2;
                height = (float) recyclerView.getHeight() / 5;

                adapter = new ItemsAdapter(getActivity(), position, itemList, itemColour, width, height, imgCart, tvCount);

                // setting adapter to our recycler view
                recyclerView.setAdapter(adapter);

                adapter.notifyDataSetChanged();
                return true;
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
    }

}