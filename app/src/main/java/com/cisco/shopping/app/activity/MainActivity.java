package com.cisco.shopping.app.activity;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.graphics.PointF;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.cisco.shopping.app.R;
import com.cisco.shopping.app.adapter.DynamicCategoriesPagerAdapter;
import com.cisco.shopping.app.helper.DB_Handler;
import com.cisco.shopping.app.helper.DepthPageTransformer;
import com.google.android.material.tabs.TabLayout;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.app.AppCompatDelegate;
import androidx.appcompat.widget.Toolbar;
import androidx.viewpager.widget.ViewPager;

public class MainActivity extends AppCompatActivity {
    private Toolbar toolbar;
    public TextView tvCount;
    public RelativeLayout lnCart;
    public ImageView imgCart;
    private TabLayout tabLayout;
    private ViewPager viewPager;
    private String sTabName="", sCount="";
    private int inPos = 0;
    private List<String> itemsNew = new ArrayList<>();
    private List<String> itemList = new ArrayList<>();
    private List<String> itemColour = new ArrayList<>();
    private List<String> categoryList;
    private ArrayList<String> tabTitles = new ArrayList<>();
    private HashMap<String, String> newList = null;
    private HashMap<String, String> newListColour = null;
    private DB_Handler db_handler;
    private int cartCount = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // set theme
        if (AppCompatDelegate.getDefaultNightMode() == AppCompatDelegate.MODE_NIGHT_YES) {
            setTheme(R.style.DarkTheme);
        } else {
            setTheme(R.style.LightTheme);
        }
        tabTitles.clear();
        initViews();

    }

    @SuppressLint("ClickableViewAccessibility")
    private void initViews() {
        // initialise the layout
        viewPager = (ViewPager) findViewById(R.id.viewpager);
        tabLayout = (TabLayout) findViewById(R.id.tabLayout);

        // initialise the toolbar
        toolbar = (Toolbar) findViewById(R.id.toolbar);

        setSupportActionBar(toolbar);

        // initialise other views
        tvCount = (TextView) findViewById(R.id.tvCount);
        imgCart = (ImageView) findViewById(R.id.imgCart);
        lnCart = (RelativeLayout) findViewById(R.id.lnCart);

        db_handler = new DB_Handler(MainActivity.this);
        cartCount = db_handler.getCartItemCount();

        if (cartCount > 0) {
            tvCount.setText(String.valueOf(cartCount));
            Log.e("###count", String.valueOf(cartCount) );
        } else {
            tvCount.setText("0");
        }

        // defines touch listener
        imgCart.setOnTouchListener(new View.OnTouchListener() {
            PointF DownPT = new PointF(); // Record Mouse Position When Pressed Down
            PointF StartPT = new PointF(); // Record Start Position of 'img'

            PointF StartTV = new PointF(); // Record Start Position of 'img'

            @Override
            public boolean onTouch(View v, MotionEvent event) {
                switch (event.getAction()) {
                    case MotionEvent.ACTION_MOVE :
                        imgCart.setX((int)(StartPT.x + event.getX() - DownPT.x));
                        imgCart.setY((int)(StartPT.y + event.getY() - DownPT.y));
                        StartPT.set( imgCart.getX(), imgCart.getY() );

                        tvCount.setX((int)(StartTV.x + event.getX() - DownPT.x));
                        tvCount.setY((int)(StartTV.y + event.getY() - DownPT.y));
                        StartTV.set( tvCount.getX(), tvCount.getY() );

                        break;
                    case MotionEvent.ACTION_DOWN :
                        DownPT.set( event.getX(), event.getY() );
                        StartPT.set( imgCart.getX(), imgCart.getY() );
                        StartTV.set( tvCount.getX(), tvCount.getY() );
                        break;
                    case MotionEvent.ACTION_UP :
                        // Nothing have to do
                        break;
                    default :
                        break;
                }
                return true;
            }
        });

        // set off-screen-page-limit (no. of tabs shown in on page)
        viewPager.setOffscreenPageLimit(5);
        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
        tabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                // set current item as the tab position
                viewPager.setCurrentItem(tab.getPosition());

                sTabName = String.valueOf(tab.getText());
                inPos = tab.getPosition();
                Log.e("##Tab", sTabName);
                Log.e("##TabPosition", String.valueOf(inPos));

                // filter data as per category
                loadTabData(sTabName, inPos);
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });

        imgCart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent inCartList = new Intent(MainActivity.this, CartItemsActivity.class);
                startActivity(inCartList);
            }
        });

        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            int pos = 0;

            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                Bundle bundle = new Bundle();
                bundle.putInt("position", position);
            }

            @Override
            public void onPageSelected(int position) {
                pos = position;
            }

            @Override
            public void onPageScrollStateChanged(int state) {
            }
        });

        // set viewpager animation
        viewPager.setPageTransformer(true, new DepthPageTransformer());

        // fetch data from json file
        loadData();

    }

    @SuppressLint("NewApi")
    private void loadData() {
        InputStream is = this.getResources().openRawResource(R.raw.shoppinglist);
        int size = 0;
        try {
            size = is.available();
        } catch (IOException e) {
            e.printStackTrace();
        }
        byte[] buffer = new byte[size];
        try {
            is.read(buffer);
        } catch (IOException e) {
            e.printStackTrace();
        }
        try {
            is.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        try {
            String json = new String(buffer, "UTF-8");
            Log.e("##JSONFile", json);

            // The response will have JSON Format String

            JSONObject jsonObjectMain = null;
            JSONArray jArray = null;
            JSONArray jArrayItems = null;

            ArrayList<JSONObject> array = new ArrayList<JSONObject>();

            try {
                jsonObjectMain = new JSONObject(json);

                // Getting JSON array node
                jArray = jsonObjectMain.getJSONArray("categories");

                // looping through the json file
                for (int i = 0; i < jArray.length(); i++) {
                    array.add(jArray.getJSONObject(i));
                }

                Collections.sort(array, new Comparator<JSONObject>() {
                    @Override
                    public int compare(JSONObject lhs, JSONObject rhs) {
                        try {
                            return (lhs.getString("name").compareTo(rhs.getString("name")));
                        } catch (JSONException e) {
                            e.printStackTrace();
                            return 0;
                        }
                    }
                });

                JSONArray jsonArray = new JSONArray(array);
                JSONObject objectItems = null;
                String sCategoryName = "", sColour = "", sItemName = "";

                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject row = jsonArray.getJSONObject(i);

                    sCategoryName = row.getString("name");
                    sColour = row.getString("colour");
                    Log.e("##CategoryName", sCategoryName);
                    Log.e("##CategoryColour", sColour);

                    jArrayItems = row.getJSONArray("items");

                    for (int j = 0; j < jArrayItems.length(); j++) {
                        objectItems = jArrayItems.getJSONObject(j);

                        sItemName = objectItems.getString("name");
                        Log.e("##ItemName", sItemName);

                        itemsNew.add(sItemName);
                    }

                    categoryList = new ArrayList<>();
                    categoryList.add(sCategoryName);

                    for (int k = 0; k < categoryList.size(); k++) {
                        // set the tab name
                        tabLayout.addTab(tabLayout.newTab().setText(categoryList.get(k)));

                        //tabLayout.setBackgroundColor(Integer.parseInt(sColour));

                        // set tab title
                        tabTitles.add(String.valueOf(categoryList));
                    }

                }
                Log.e("##Data4", String.valueOf(itemsNew));

                setDynamicFragmentToTabLayout();

            } catch (JSONException e) {
                e.printStackTrace();
            }
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
    }

    private void setDynamicFragmentToTabLayout() {
        DynamicCategoriesPagerAdapter adapter = new DynamicCategoriesPagerAdapter(
                getSupportFragmentManager(), tabLayout.getTabCount(), itemList, itemColour, imgCart, tvCount);

         // set the adapter
        viewPager.setAdapter(adapter);
    }

    private void loadTabData(String sTabName, int inPos) {
        InputStream is = this.getResources().openRawResource(R.raw.shoppinglist);
        int size = 0;
        try {
            size = is.available();
        } catch (IOException e) {
            e.printStackTrace();
        }
        byte[] buffer = new byte[size];
        try {
            is.read(buffer);
        } catch (IOException e) {
            e.printStackTrace();
        }
        try {
            is.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        try {
            String json = new String(buffer, "UTF-8");
            Log.e("##JSONFile", json);

            // The response will have JSON Format String

            JSONObject jsonObjectMain = null;
            JSONArray jArray = null;
            JSONArray jArrayItems = null;

            ArrayList<JSONObject> array = new ArrayList<JSONObject>();

            try {
                jsonObjectMain = new JSONObject(json);

                // Getting JSON array node
                jArray = jsonObjectMain.getJSONArray("categories");

                // looping through the json file
                for (int i = 0; i < jArray.length(); i++) {
                    array.add(jArray.getJSONObject(i));
                }

                Collections.sort(array, new Comparator<JSONObject>() {
                    @Override
                    public int compare(JSONObject lhs, JSONObject rhs) {
                        try {
                            return (lhs.getString("name").compareTo(rhs.getString("name")));
                        } catch (JSONException e) {
                            e.printStackTrace();
                            return 0;
                        }
                    }
                });

                JSONArray jsonArray = new JSONArray(array);
                JSONObject objectItems = null;
                String sCategoryName = "", sColour = "", sItemName = "";

                itemList.clear();
                itemColour.clear();

                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject row = jsonArray.getJSONObject(i);

                    sCategoryName = row.getString("name");
                    sColour = row.getString("colour");
                    Log.e("##CategoryName", sCategoryName);
                    Log.e("##CategoryColour", sColour);

                    newList = new HashMap<>();
                    newListColour = new HashMap<>();

                    // items node (array) in JSON Object
                    jArrayItems = row.getJSONArray("items");

                    for (int j = 0; j < jArrayItems.length(); j++) {
                        objectItems = jArrayItems.getJSONObject(j);

                        sItemName = objectItems.getString("name");
                        Log.e("##ItemName", sItemName);

                        newList.put(sCategoryName, sItemName);

                        for (Map.Entry<String,String> entry : newList.entrySet()) {
                            Log.e("##Values", entry.getKey() +" : "+ entry.getValue());

                            if(entry.getKey().equals(sTabName)){
                                Log.e("##Values2", entry.getValue());
                                itemList.add(entry.getValue());
                            }
                        }

                        newListColour.put(sCategoryName, sColour);

                        for (Map.Entry<String,String> entry : newListColour.entrySet()) {
                            Log.e("##Values", entry.getKey() +" : "+ entry.getValue());

                            if(entry.getKey().equals(sTabName)){
                                Log.e("##Values2", entry.getValue());
                                itemColour.add(entry.getValue());
                            }
                        }

                        Log.e("##Values3", String.valueOf(itemList));
                        Log.e("##Values4", String.valueOf(itemColour));
                    }

                    setDynamicFragmentToTabLayoutNew();

                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
    }

    private void setDynamicFragmentToTabLayoutNew() {
        DynamicCategoriesPagerAdapter adapter = new DynamicCategoriesPagerAdapter(
                getSupportFragmentManager(), tabLayout.getTabCount(), itemList, itemColour, imgCart, tvCount);

        // set the adapter
        viewPager.setAdapter(adapter);

        // set the current item position
        viewPager.setCurrentItem(inPos);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_theme, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here.
        int id = item.getItemId();

        if (id == R.id.action_theme) {
            if (AppCompatDelegate.getDefaultNightMode() == AppCompatDelegate.MODE_NIGHT_YES) {
                AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO);
            } else {
                AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_YES);
            }

            finish();
            startActivity(new Intent(MainActivity.this, MainActivity.this.getClass()));
            return true;
        }
        return super.onOptionsItemSelected(item);
    }


}