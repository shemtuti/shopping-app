package com.cisco.shopping.app.adapter;

public class ItemsInCart {
    private String name;

    public ItemsInCart(String name) {
        this.name = name;
    }

    public String getName() {
        return this.name;
    }
}
