package com.cisco.shopping.app.adapter;

import android.widget.ImageView;
import android.widget.TextView;

import com.cisco.shopping.app.fragment.DynamicCategoryFragment;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;

public class DynamicCategoriesPagerAdapter extends FragmentStatePagerAdapter {
    int numOfTabs;
    private List<String> itemList = new ArrayList<>();
    private List<String> itemColour = new ArrayList<>();
    private ImageView imgCart;
    private TextView tvCount;

    public DynamicCategoriesPagerAdapter(FragmentManager fragmentManager, int numOfTabs,
                                         List<String> itemList, List<String> itemColour, ImageView imgCart, TextView tvCount) {
        super(fragmentManager);
        this.numOfTabs = numOfTabs;
        this.itemList = itemList;
        this.itemColour = itemColour;
        this.imgCart = imgCart;
        this.tvCount = tvCount;
    }

    @Override
    public int getCount() {
        return numOfTabs;
    }

    @NonNull
    @Override
    public Fragment getItem(int position) {
        return DynamicCategoryFragment.getInstance(position, itemList, itemColour, imgCart, tvCount);
    }
}
