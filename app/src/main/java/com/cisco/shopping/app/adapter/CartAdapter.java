package com.cisco.shopping.app.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.cisco.shopping.app.R;

import java.util.ArrayList;

public class CartAdapter extends BaseAdapter {

    private ArrayList<ItemsInCart> itemList;
    private Context context;

    public CartAdapter(ArrayList<ItemsInCart> list, Context cont){
        this.itemList = list;
        this.context = cont;
    }

    @Override
    public int getCount() {
        return this.itemList.size();
    }

    @Override
    public Object getItem(int position) {
        return this.itemList.get(position);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder = null;

        if(convertView == null){
            LayoutInflater inf = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inf.inflate(R.layout.cart_item, null);

            holder = new ViewHolder();
            holder.name = (TextView)convertView.findViewById(R.id.tvItemName);

            convertView.setTag(holder);
        }
        else {
            holder = (ViewHolder)convertView.getTag();
        }

        ItemsInCart item = itemList.get(position);
        holder.name.setText(item.getName());

        return convertView;
    }

    private static class ViewHolder{
        public TextView name;
    }
}

